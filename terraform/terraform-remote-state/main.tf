# https://github.com/nozaq/terraform-aws-remote-state-s3-backend

module "remote_state" {
  source = "./modules/terraform-aws-remote-state-s3-backend/"

  providers = {
    aws         = aws
    aws.replica = aws.replica
  }
}

resource "aws_iam_user" "terraform" {
  name = "TerraformUser"
}

resource "aws_iam_user_policy_attachment" "remote_state_access" {
  user       = aws_iam_user.terraform.name
  policy_arn = module.remote_state.terraform_iam_policy.arn
}
