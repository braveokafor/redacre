output "kms_key_id" {
  description = "The KMS customer master key to encrypt state buckets."
  value       = module.remote_state.kms_key.key_id
}

output "bucket" {
  description = "The S3 bucket to store the remote state file."
  value       = module.remote_state.state_bucket.bucket
}

output "dynamodb_table" {
  description = "THE_ID_OF_THE_DYNAMODB_TABLE"
  value       = module.remote_state.dynamodb_table.id
}
