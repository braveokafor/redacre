# General
variable "region" {
  type    = string
  default = "us-east-1"
}

variable "tags" {
  type = map(string)
  default = {
    "environment" = "dev"
  }
}

# EKS
variable "cluster_name" {
  type = string
}

variable "cluster_version" {
  type    = string
  default = "1.22"
}

variable "core_dns_node_group_machine_type" {
  type    = list(string)
  default = ["t3.large"]
}