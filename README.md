<h1 align="center">
  Redacre DevOps test.
</h1>
<h3 align="center">These scripts setup a simple ReactJS app on Docker/ Kubernetes.</h3>

## <p align="center">⚡️ How it Works</p>

<p>
<h2 align="center">Task 1 - Dockerize the Application.</h2>

|                                         Action                                                |          file              |
|-----------------------------------------------------------------------------------------------|----------------------------|
| Creates a [Docker](https://docs.docker.com/get-started/) image for the Python API.            |`./app/api/Dockerfile`      | 
| Creates a [Docker](https://docs.docker.com/get-started/) image for the ReactJS frontend.      |`./app/sys-stats/Dockerfile`| 
| Creates a [Docker](https://docs.docker.com/get-started/) image for the Nginx reverse proxy.   |`./app/nginx/Dockerfile`    | 
| Start / orchestrate all 3 containeres using [Docker compose](https://docs.docker.com/compose/) |`./docker-compose.yaml`     | 

#### 🛠 Usage
- Clone this repo `git clone https://gitlab.com/braveokafor/redacre.git`. 
- Navigate to the `./redacre` directory. 

To use the pre-built Docker images:
- Run `docker-compose up`.

To build your own Docker images:
- Comment out the `image:` field, then uncomment the `build:` and `context:` fields in the `./docker-compose.yaml` file.
- Your final `docker-compose.yaml` file should look like the YAML script below.
- Run `docker-compose up`.

```yaml
version: "3.8"
services:
  nginx:
    #image: braveokafor/redacre-nginx
    build:
      context: ./nginx
    ports:
        - 80:80
    restart: always
    depends_on:
      - ui
      - api
  ui:
    #image: braveokafor/redacre-ui
    build:
      context: ./sys-stats
  api:
    #image: braveokafor/redacre-api
    build:
      context: ./api
```
</p>

<p align="center">
<h2>Task 2 - Deploy on Cloud</h2>

|                                                             Action                                                                                                                                                                                                                                                                                               |               tf file                |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| Creates a [VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)                                                                                                                                                                                                                                                                        |`./terraform/main.tf` #module.vpc     | 
| Creates public and private [Subnets](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html) across all 3 [Availability Zones](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html) in the specified [Region](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html)  |`./terraform/main.tf` #module.vpc     | 
| Creates a Single VM Managed [Node Group](https://docs.aws.amazon.com/eks/latest/userguide/managed-node-groups.html), Required for [CoreDns](https://docs.aws.amazon.com/eks/latest/userguide/managing-coredns.html)                                                                                                                                              |`./terraform/main.tf` #module.eks     | 
| Creates a Fargate [profile ](https://docs.aws.amazon.com/eks/latest/userguide/fargate-profile.html)                                                                                                                                                                                                                                                              |`./terraform/main.tf` #module.eks     | 
| Creates a [Fargate](https://docs.aws.amazon.com/eks/latest/userguide/fargate.html) based [EKS](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html) cluster                                                                                                                                                                                        |`./terraform/main.tf` #module.eks     | 
| Creates a [KMS](https://docs.aws.amazon.com/kms/) key for the EKS cluster                                                                                                                                                                                                                                                                                        |`./terraform/main.tf` #aws_kms_key.key| 

#### 🛠 Usage
- Clone this repo `git clone https://gitlab.com/braveokafor/redacre.git`. 
- Navigate to the `./redacre/terraform` directory.  
- Edit the `cluster_name` and `region` variables in the `terraform.tfvars` file.
- Your final `terraform.tfvars` file should look similar to the script below.
```hcl
cluster_name = "redacre"
region       = "eu-west-2"
```
Then perform the following commands on the directory:
- `terraform init` to get required plugins.
- `terraform plan` to review planned resources before creation.
- `terraform apply` to apply provision resources.
</p>

<p>
<h2 align="center">Task 3 - Get it to work with Kubernetes.</h2>

|                                         Action                                                                                                                                                                                                           |          file                           |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------|
| Creates the required [Deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) (ui, api, and reverseproxy).                                                                                                                   |`./k8s/deployment/*`                     | 
| Creates the required [Services](https://kubernetes.io/docs/concepts/services-networking/service/) for the deployments.                                                                                                                                   |`./k8s/services/*`                       | 
| Creates a [LoadBalancer](https://kubernetes.io/docs/concepts/services-networking/service/) (routable public IP) for the Nginx proxy.                                                                                                                     |`./k8s/services/nginx.yaml`              | 
| Creates a [ServiceAccount](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/) required by the UI deployment to read from the API (internal DNS not routing on the Static React app, both on "docker compose" and K8s). |`./k8s/serviceaccount/read-services.yaml`| 
| Creates a [Role](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) get services running on the Cluster                                                                                                                                      |`./k8s/role/read-services.yaml`          | 
| Creates a [RoleBinding](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#rolebinding-and-clusterrolebinding) to bind the Role to the Service Account                                                                                        |`./k8s/rolebinding/read-services.yaml`   | 

#### 🛠 Usage
- Clone this repo `git clone https://gitlab.com/braveokafor/redacre.git`. 
- Navigate to the `./redacre/k8s` directory. 
- Run `kubectl apply -f ./service --recursive` to provision the required services. 
- Check that the `nginx` service has a routable IP address/FQDN: 
- `kubectl get services -o wide | grep nginx | awk '{print $4}`. 
- NB: The React App deployment requires a routable nginx IP.
- Run `kubectl apply -f . --recursive` to provision the remaining resources.
</p>
