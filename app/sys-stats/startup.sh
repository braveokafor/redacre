#!/bin/sh

# substitute NGINX_URL for "nginx service" IP, if running on k8s
# http://nginx not working with the React App.
# process.ENV not a viable option in either (static html)

if [ $(kubectl get services -o wide | grep nginx | awk '{print $4}') ]; then    
    grep -rl NGINX_URL . | xargs sed -i "s|NGINX_URL|$(kubectl get services -o wide | grep nginx | awk '{print $4}')|g"
else 
    grep -rl NGINX_URL . | xargs sed -i "s|NGINX_URL|localhost|g"
fi

nginx -g 'daemon off;'